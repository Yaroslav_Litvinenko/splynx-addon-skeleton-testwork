<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public function getAddOnTitle()
    {
        return 'Splynx Add-on Skeleton';
    }

    public function getModuleName()
    {
        return 'splynx_addon_skeleton';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
            ],
        ];
    }

    public function getEntryPoints()
    {
        return [
            [
                'name' => 'skeleton_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Fskeleton',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }
}
