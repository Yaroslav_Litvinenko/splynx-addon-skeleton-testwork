Splynx Add-on Skeleton
======================

Splynx Add-on Skeleton based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.4.2"
composer install
~~~

Install Splynx Add-on Skeleton:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-skeleton.git
cd splynx-addon-skeleton
composer install
sudo chmod +x yii
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-skeleton/web/ /var/www/splynx/web/skeleton
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-skeleton.addons
~~~

with following content:
~~~
location /skeleton
{
        try_files $uri $uri/ /skeleton/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

You can then access Splynx Add-On Skeleton in menu "Customers".