<?php

namespace app\models;

use splynx\helpers\ApiHelper;

class Customer extends \splynx\models\Customer
{
    public static function getFiveLastAdded()
    {
        $result = ApiHelper::getInstance()->search(self::$apiCall, [
            'order' => ['id' => 'DESC'],
            'limit' => 5
        ]);

        if ($result['result'] == false or empty($result['response'])) {
            return [];
        }

        $models = [];
        foreach ($result['response'] as $row) {
            $model = new static();
            static::populate($model, $row);
            $models[] = $model;
        }

        return $models;
    }
}
